'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const util = require('util');
const fs = require("fs");
const Guid = require("guid");
const Promise = require("promise");

//Json Responds files
const employees = require("./data/employees");
const stats = require("./data/statistic");

const rootAPI = '/api/v1';
const PORT = process.env.PORT || 3000;
console.log(process.version)
app.use(bodyParser.json());

// on Empty Path
app.get(rootAPI, function (req, res) {
    console.log('Get All employees');
    res.send("Try /api/v1/employees")
});

// on Empty Path
app.get('/', function (req, res) {
    console.log('Get All employees');
    res.send("Try /api/v1/employees")
});

function readFile(filename, enc) {
    return new Promise(function (fulfill, reject) {
        fs.readFile(filename, enc, function (err, res) {
            if (err) reject(err);
            else fulfill(res);
        });
    });
}

// Get All Employees
app.get(rootAPI + '/employees', function (req, res) {
    console.log('Get All employees');
    let jsonArray = JSON.parse(fs.readFileSync('./data/employees.json'));
    if (jsonArray.length > 0) {
        let response = [];
        jsonArray.map(function (obj) {
            response.push({
                "id": obj.id,
                "firstName": obj.firstName,
                "lastName": obj.lastName,
                "email": obj.email
            })
        })
        res
            .status(200)
            .json(response);

    }
    //TODO: This is probably working, but what if the json array has millions of records:
    // Try to make it synchronuous

});

function readFile(filename) {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, (err, data) => {
            return err ? reject(err) : resolve(data);
        });
    });
}

//TODO  Get Statistic : Write the GET function to read from static json file and
// respond a json array including all the objects
app.get(rootAPI + '/statistics', function (req, res) {
    console.log("Get all statistics");
    readFile('./data/statistic.json')
        .then(data => res.status(200).json(JSON.parse(data)))
        .catch(err => res.status(404).end())

});


// Get one Employees on ID
app.get(rootAPI + '/employees/:id', function (req, res) {
    console.log('Get an employee based on id');
    // TODO get the id from request, find the object with id, return the object
    // With all of the properties
    const id = String(req.params.id);
    new Promise(function (resolve, reject) {
        fs.readFile('./data/employees.json', function (err, data) {
            let jsonArray = JSON.parse(data);
            jsonArray.find(function (employee, index) {
                if (employee.id === id) {
                    resolve(employee);
                }
                if (index === jsonArray.length - 1) {
                    reject();
                }
            });
        });
    })
        .then(function (result) {
            res
                .status(200)
                .json(result);
        })
        .catch(function (err) {
            res
                .status(404)
                .end();
        });
});

// TODO Write the POST function to get an object from Request Body, add it to
// employees json array (file) The object should have all the elements
app.post(rootAPI + '/employees', function (req, res) {

    if (req.body.firstName && req.body.lastName && req.body.email && req.body.gender && Number(req.body.age) && String(req.body.startDate), req.body.image) {
        console.log("Creating new employee");
        fs.readFile('./data/employees.json', function (err, data) {
            let jsonArray = JSON.parse(data);
            let objectToPush = {
                "id" : Guid.raw(),
                "firstName" : req.body.firstName,
                "lastName" : req.body.lastName,
                "email": req.body.email,
                "gender" : req.body.gender,
                "age" : req.body.age,
                "startDate" : req.body.startDate,
                "image" : req.body.image
            }
            jsonArray.push(objectToPush);
            fs.writeFile('./data/employees.json', JSON.stringify(jsonArray), function(err){
                if(err){
                    console.log(err);
                }
                res.status(201).json(objectToPush);
            });
            
        });
    }

});

// TODO Write the Delete function to get an id from URL, delete the object
// related to that id from json file
app.delete(rootAPI + '/employees/:id', function (req, res) {
    var id = req.params.id;
    fs.readFile('./data/employees.json', function (err, data) {
        let jsonArray = JSON.parse(data);
        jsonArray = jsonArray.filter(elem => elem.id != id);
        fs.writeFile('./data/employees.json', JSON.stringify(jsonArray));
        res.status(200).send('');
    });
});

// TODO Write the PUT function to get an object (can be one or couple of
// elements) from Request Body , id from request URL, And modify the related
// object in the json file Running API Server
app.put(rootAPI + '/employees/:id', function (req, res) {
    console.log('PUT employee with id');

    var employee = req.body;
    var id = req.params.id;

    fs.readFile('./data/employees.json', function (err, data) {
        let jsonArray = JSON.parse(data);
        var index = jsonArray.findIndex(elem => elem.id == req.params.id);

        if (index != -1) {
            jsonArray[index] = employee;
            fs.writeFile('./data/employees.json', JSON.stringify(jsonArray));
            res
                .status(200)
                .send();
        } else {
            res.status(404).send("ID not valid");
        }

    });


});



var serverHost = app.listen(PORT, function () {
    const host = serverHost
        .address()
        .address;
    const port = serverHost
        .address()
        .port;
    console.log('Backend running on ' + host + ':' + port);
});