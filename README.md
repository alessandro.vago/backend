**Academy day 1 - Backend LAB**

This repository contains the skeleton of the project which you can start the development of the backend platform

## Prerequisites

* **Git local installation**
* **Gitlab Account**
* **Nodejs & npm**
* **Sap Cloud Platform Account**
* **CLOUD FOUNDRY CLI local installation**

## Local development
Open terminal and type the following commands

* **Clone repository**:

```shell
$ git clone https://gitlab.com/techedge-academy/backend.git
```
Put your Gitlab credentials.

* **Npm initialize**:

```shell
$ npm install
```

* **Start application**:

```shell
$ node server.js
```
This command start the application on http://localhost:3000

* **Test application**:

Go to http://localhost:3000 with your browser and check if it responds with "Try /api/v1/employees" and then try the path.

- Each one of you should work on his/her git repository so you need to commit and push the existing project to your repository or you can fork it on Gitlab (especialy in the cases that you want to contribute to existing project). We assume that you change the remote URL and push it to your repository.
- So first, create your repository project on the gitlab, call it "backend". then copy the url and continue:


* **Change origin repository**:

```shell
$ git remote set-url origin {YOUR_REPO}
```

Check if update works

```shell
$ git remote -v
```
* **Push to your repository**:

```shell
$git push -u origin master
```
## TODO

*now you have skeleton of the project locally and remotely on your gitlab account, let's code and commit your code to git*

- Go to **server.js** file and checkout TODO comments.
- After you implemented the missing parts, first check it locally with POSTMAN make sure it is working, then commit your code to your repository.

## Deployment on SAP Cloud Foundry

- Find you API endpoint on the platform
![alt text](screenshot-api.png
 "API Endpoint")

 - ```$ cf api [The link which you found above] ```

 - Login to your SAP CF account using your credential and push it. 
 ```
 $ cf login 
 $ cf push
 ``` 

- Go to the SAP CF platform and find your app in application section
- Go to its URL and check if your API responds correctly with POSTMAN